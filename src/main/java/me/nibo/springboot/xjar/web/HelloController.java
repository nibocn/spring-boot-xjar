package me.nibo.springboot.xjar.web;

import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.Maps;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/hello")
@Slf4j
public class HelloController {

    @GetMapping("{name}")
    public ResponseEntity<Map<String, Object>> say(@PathVariable String name) {
        Map<String, Object> result = new HashMap<>();
        log.info("用户名：{}", name);
        result.put("msg", "Hello: " + name);
        result.put("status", "success");
        return ResponseEntity.ok(result);
    }

}
